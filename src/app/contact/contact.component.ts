import { Component, OnInit } from '@angular/core';
import * as app from 'tns-core-modules/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import * as Email from 'nativescript-email';
import * as Phone from 'nativescript-phone';

@Component({
    selector: 'app-contact',
    moduleId: module.id,
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {

    constructor(private fonticon: TNSFontIconService) { }

    ngOnInit() { }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    sendEmail() {

        Email.available()
            .then ((avail: boolean) => {
                if (avail) {
                    Email.compose({
                        to: ['cssupport@grandcru.com'],
                        subject: '[Grand Cru Cellar]: Enquiry',
                        body: 'Dear Sir/Madam,'
                    });
                }
                else {
                    console.log('No Email Configured.');
                }
            });
    }

    callShop(phoneNum: string) {
        
        Phone.dial(phoneNum, true);
    }
}