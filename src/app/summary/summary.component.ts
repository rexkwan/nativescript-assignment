import { Component, OnInit, Inject } from '@angular/core';
import { ModalDialogService, ModalDialogOptions, ModalDialogParams } from '@nativescript/angular/modal-dialog';
import { WineSummary } from '../shared/winesummary';

@Component({
    selector: 'app-summary',
    moduleId: module.id,
    templateUrl: './summary.component.html'
})

export class SummaryModalComponent implements OnInit {

    wineSummary: WineSummary;

    constructor(private params: ModalDialogParams,
        @Inject('BaseURL') private BaseURL) { }

    ngOnInit() {
        this.wineSummary = this.params.context;
    }

    goBack(): void {
        this.params.closeCallback();
    }
}