import { Component, OnInit, Inject } from '@angular/core';
import * as app from 'tns-core-modules/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

import { Promotion } from '../shared/promotion';
import { PromotionService } from '../services/promotion.service';

@Component({
    selector: 'app-promotion',
    moduleId: module.id,
    templateUrl: './promotion.component.html',
    styleUrls: ['./promotion.component.css']
})

export class PromotionComponent implements OnInit {

    promotions: Promotion[];
    errMess: string;

    constructor(private promotionService: PromotionService,
        @Inject('BaseURL') private BaseURL) { }

    ngOnInit() {
        this.promotionService.getPromotions()
            .subscribe(promotions => this.promotions = promotions,
                errmess => this.errMess = <any>errmess);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}