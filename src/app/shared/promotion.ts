export interface Promotion {
    id: string;
    title: string;
    offer: string;
    wineid: string;
    winename: string;
    image: string;
    detail: string;
    supplement: string;
    expiration: string;
}