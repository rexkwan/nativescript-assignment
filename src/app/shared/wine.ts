import { Winedetail } from './winedetail';
import { Overview } from './overview';

export interface Wine {
    id: string;
    name: string;
    abbr: string;
    image: string;
    producer: string;
    country: string;
    hotitem: boolean;
    winedetail: Winedetail[];
    overview: Overview[];
}