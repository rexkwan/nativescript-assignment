import { Winedetail } from './winedetail';
import { Overview } from './overview';

export interface WineSummary {
    id: string;
    name: string;
    producername: string;
    country: string;
    overview: Overview[];
}