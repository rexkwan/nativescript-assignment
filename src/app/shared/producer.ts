import { Producerdetail } from './producerdetail';

export interface Producer {
    id: string;
    name: string;
    image: string;
    website: string;
    producerdetail: Producerdetail[];
}