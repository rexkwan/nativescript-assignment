import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { UserAuthComponent } from "./userauth/userauth.component";
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PromotionComponent } from './promotion/promotion.component';
import { ContactComponent } from "./contact/contact.component";
import { ProductsComponent } from './products/products.component';
import { FaqComponent } from "./faq/faq.component";
import { WinedetailComponent } from './winedetail/winedetail.component';

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "userauth", component: UserAuthComponent },
    { path: "home", component: HomeComponent },
    { path: "about", component: AboutComponent },
    { path: "promotion", component: PromotionComponent },
    { path: "contact", component: ContactComponent },
    { path: "faq", component: FaqComponent },
    { path: "products/:country", component: ProductsComponent },
    { path: "winedetail/:id", component: WinedetailComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
