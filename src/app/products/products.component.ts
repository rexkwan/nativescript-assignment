import { Component, OnInit,Inject } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import * as app from 'tns-core-modules/application';

import { Wine } from '../shared/wine';
import { WineService } from '../services/wine.service';

@Component({
    selector: 'app-products',
    moduleId: module.id,
    templateUrl: './products.component.html',
    styleUrls: ['/products.component.css']
})

export class ProductsComponent implements OnInit {

    wines: Wine[];
    errMess: string;
    country: string;

    constructor(private wineService: WineService,
        private route: ActivatedRoute,
        @Inject('BaseURL') private BaseURL) { }

    ngOnInit() {
        this.route.params
            .subscribe(params => this.country = params['country']);

        if (this.country == 'All Region') {
            this.wineService.getWines()
                .subscribe(wines => this.wines = wines,
                    errmess => this.errMess = <any>errmess);
        }
        else {
            this.wineService.getCtryWines(this.country)
                .subscribe(wines => this.wines = wines,
                    errmess => this.errMess = <any>errmess);
        }
    }

    getWines() {
        if (this.country == 'All Region') {
            this.wineService.getWines()
                .subscribe(wines => this.wines = wines,
                    errmess => this.errMess = <any>errmess);
        }
        else {
            this.wineService.getCtryWines(this.country)
                .subscribe(wines => this.wines = wines,
                    errmess => this.errMess = <any>errmess);
        }        
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

}