import { Component } from '@angular/core';
import * as app from 'tns-core-modules/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
    selector: 'app-faq',
    moduleId: module.id,
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.css']
})

export class FaqComponent {

    constructor() { }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}