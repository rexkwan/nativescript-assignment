import { Component, OnInit, Inject } from '@angular/core';
import { ModalDialogService, ModalDialogOptions, ModalDialogParams } from '@nativescript/angular/modal-dialog';
import { Producer } from '../shared/producer';

@Component({
    selector: 'app-producer',
    moduleId: module.id,
    templateUrl: './producer.component.html'
})

export class ProducerModalComponent implements OnInit {

    producer: Producer;

    constructor(private params: ModalDialogParams,
        @Inject('BaseURL') private BaseURL) { }

    ngOnInit() { 
        this.producer = this.params.context;
    }

    goBack(): void {
        this.params.closeCallback();
    }
}