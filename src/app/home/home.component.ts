import { Component, OnInit, Inject } from '@angular/core';
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import { Page } from 'tns-core-modules/ui/page';
import { View } from 'tns-core-modules/ui/core/view';
import { SwipeGestureEventData, SwipeDirection } from 'tns-core-modules/ui/gestures';
import * as enums from 'tns-core-modules/ui/enums';
import * as app from 'tns-core-modules/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

import { Wine } from '../shared/wine';
import { Promotion } from '../shared/promotion';

import { WineService } from '../services/wine.service';
import { PromotionService } from '../services/promotion.service';

@Component({
    selector: 'app-home',
    moduleId: module.id,
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

    wines: Wine[];
    promotion: Promotion;
    wineErrMsg: string;
    promoErrMsg: string;
    numOfCards: number = 3;
    cardLabel: string[] = [];
    showCard: boolean[] = [];
    cardId: number = 0;
    index: number = 0;
    xCoord: number = 0;
    midPoint: number = this.numOfCards / 2;
    card: View;

    constructor(private wineService: WineService,
        private promotionService: PromotionService,
        private page: Page,
        private fonticon: TNSFontIconService,
        @Inject('BaseURL') private BaseURL) {

            for ( this.index = 0; this.index < this.numOfCards; this.index = this.index + 1) {
                this.cardLabel[this.index] = "card" + this.index.toString();
                this.showCard[this.index] = false;
            }
            this.showCard[0] = true;

        }

    ngOnInit() {

        this.wineService.getHotItems()
            .subscribe(wines => this.wines = wines,
                errmess => this.wineErrMsg = <any>errmess);
        
        this.promotionService.getPromotions()
            .subscribe(promotions => this.promotion = promotions[0],
                errmess => this.promoErrMsg = <any>errmess);

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onSwipe(args: SwipeGestureEventData) {

        this.showCard[this.cardId] = false;
        if (args.direction === SwipeDirection.left) {
            this.cardId = (this.cardId + this.numOfCards + 1)%this.numOfCards;
        }
        else if (args.direction === SwipeDirection.right) {
            this.cardId = (this.cardId + this.numOfCards - 1)%this.numOfCards;
        }

        for (this.index = 0; this.index < this.numOfCards; this.index = this.index + 1) {
            this.xCoord = this.index - this.cardId;
            
            if (Math.abs(this.xCoord) > this.midPoint) {
                if (this.xCoord > 0) {
                    this.xCoord = this.xCoord - this.numOfCards;
                }
                else {
                    this.xCoord = this.xCoord + this.numOfCards;
                }
            }

            if (this.xCoord < 0) {
                this.xCoord = -2000 + (this.xCoord + 1) * 200;
            }
            else if (this.xCoord > 0) {
                this.xCoord = 2000 + (this.xCoord - 1) * 200;
            }

            this.card = this.page.getViewById<View>(this.cardLabel[this.index]);
            this.card.animate({
                translate: { x: this.xCoord, y: 0 },
                duration: 500,
                curve: enums.AnimationCurve.easeInOut
            });
        }

        this.showCard[this.cardId] = true;

    }

}