import { Component, OnInit, Inject, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular/router';
import { ModalDialogService, ModalDialogOptions } from '@nativescript/angular/modal-dialog';
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import { action } from 'tns-core-modules/ui/dialogs';
import { switchMap } from 'rxjs/operators';

import { SummaryModalComponent } from '../summary/summary.component';
import { ProducerModalComponent } from '../producer/producer.component';

import { Wine } from '../shared/wine';
import { WineSummary } from '../shared/winesummary';
import { Producer } from '../shared/producer';

import { WineService } from '../services/wine.service';
import { ProducerService } from '../services/producer.service';

@Component({
    selector: 'app-winedetail',
    moduleId: module.id,
    templateUrl: './winedetail.component.html',
    styleUrls: ['./winedetail.component.css']
})

export class WinedetailComponent implements OnInit {

    wine: Wine;
    wineSummary: WineSummary = { 'id': '', 'name': '', 'producername': '', 'country': '', 'overview': [{ 'type': '', 'character': '' }] };
    producer: Producer;
    wineErrMsg: string;
    producerErrMsg: string;

    constructor(private wineService: WineService,
        private producerService: ProducerService,
        private route: ActivatedRoute,
        private routerExtensions: RouterExtensions,
        private modalService: ModalDialogService,
        private vcRef: ViewContainerRef,
        private fonticon: TNSFontIconService,
        @Inject('BaseURL') private BaseURL) { }

    ngOnInit() {

        this.route.params
            .pipe(switchMap((params: Params) => this.wineService.getWine(params['id'])))
            .subscribe(wine => {
                this.wine = wine;
                this.producerService.getProducer(this.wine.producer)
                    .subscribe(producer => this.producer = producer,
                        errmess => { this.producer = null; this.producerErrMsg = <any>errmess; });
                },
                errmess => { this.wine = null; this.producer = null; this.wineErrMsg = <any>errmess; });

    }

    goBack(): void {
        this.routerExtensions.back();
    }

    showActionItems(): void {

        let options = {
            title: "Actions",
            cancelButtonText: "Cancel",
            actions: ["Wine Characteristic", "Producer Information"]
        };

        action(options)
            .then ((result) => {
                if (result === "Wine Characteristic") {
                    this.wineSummary.id = this.wine.id;
                    this.wineSummary.name = this.wine.name;
                    this.wineSummary.country = this.wine.country;
                    this.wineSummary.overview = this.wine.overview;
                    this.wineSummary.producername = this.producer.name;
                    console.log(this.wineSummary.name);
                    this.showSummary(this.wineSummary);
                }
                else if (result === "Producer Information") {
                    this.showProducer(this.producer);
                }
            });
    }

    showSummary(args) {
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: args,
            fullscreen: true
        };

        this.modalService.showModal(SummaryModalComponent, options);

    }

    showProducer(args) {
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: args,
            fullscreen: true
        };

        this.modalService.showModal(ProducerModalComponent, options);

    }
}