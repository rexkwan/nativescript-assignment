import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

import { Promotion } from '../shared/promotion';
import { baseURL } from '../shared/baseurl';

@Injectable({
    providedIn: 'root'
})

export class PromotionService {

    constructor(private http: HttpClient,
        private processHTTPMsgService: ProcessHTTPMsgService) { }

    getPromotions(): Observable<Promotion[]> {
        return this.http.get<Promotion[]>(baseURL + 'promotions')
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }));
    }
}