import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

import { Wine } from '../shared/wine';
import { baseURL } from '../shared/baseurl';

@Injectable({
    providedIn: 'root'
})

export class WineService {

    constructor(private http: HttpClient,
        private processHTTPMsgService: ProcessHTTPMsgService) { }

    getWines(): Observable<Wine[]> {
        return this.http.get<Wine[]>(baseURL + 'wines')
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }));
    }

    getWine(id: string): Observable<Wine> {
        return this.http.get<Wine>(baseURL + 'wines/' + id)
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }));
    }

    getCtryWines(ctry: string): Observable<Wine[]> {
        return this.http.get<Wine[]>(baseURL + 'wines?country=' + ctry)
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }))
    }

    getHotItems(): Observable<Wine[]> {
        return this.http.get<Wine[]>(baseURL + 'wines?hotitem=true')
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }));
    }

    getWineList(ctry: string) {
        if (ctry == 'All Region') {
            return this.getWines();
        }
        else {
            return this.getCtryWines(ctry);
        }
    }
}