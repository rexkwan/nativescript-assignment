import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

import { Producer } from '../shared/producer';
import { baseURL } from '../shared/baseurl';

@Injectable({
    providedIn: 'root'
})

export class ProducerService {

    constructor(private http: HttpClient,
        private processHTTPMsgService: ProcessHTTPMsgService) { }

    getProducer(id: string): Observable<Producer> {
        return this.http.get<Producer>(baseURL + 'producers/' + id)
            .pipe(catchError(error => { return this.processHTTPMsgService.handleError(error); }));
    }
}