import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "@nativescript/angular";
import { HttpClientModule } from '@angular/common/http';
import { NativeScriptHttpClientModule } from '@nativescript/angular/http-client';
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular';
import { NativeScriptUIListViewModule } from 'nativescript-ui-listview/angular';
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { NativeScriptFormsModule } from '@nativescript/angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UserAuthComponent } from './userauth/userauth.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { AboutComponent } from './about/about.component';
import { PromotionComponent } from './promotion/promotion.component';
import { ContactComponent } from './contact/contact.component';
import { FaqComponent } from './faq/faq.component';
import { WinedetailComponent } from './winedetail/winedetail.component';
import { SummaryModalComponent } from './summary/summary.component';
import { ProducerModalComponent } from './producer/producer.component';

import { ProcessHTTPMsgService } from './services/process-httpmsg.service';
import { WineService } from './services/wine.service';
import { ProducerService } from './services/producer.service';
import { PromotionService } from './services/promotion.service';

import { baseURL } from './shared/baseurl';

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpClientModule,
        NativeScriptHttpClientModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        ReactiveFormsModule,
        TNSFontIconModule.forRoot({
            fa: require('../fonts/font-awesome.min.css')
        })
    ],
    declarations: [
        AppComponent,
        UserAuthComponent,
        HomeComponent,
        ProductsComponent,
        AboutComponent,
        PromotionComponent,
        ContactComponent,
        FaqComponent,
        WinedetailComponent,
        SummaryModalComponent,
        ProducerModalComponent
    ],
    providers: [
        {provide: 'BaseURL', useValue: baseURL},
        ProcessHTTPMsgService,
        WineService,
        ProducerService,
        PromotionService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
